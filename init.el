;;; .emacs --- My Config
;;; Commentary:
;;; Code:

;; (package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-start nil)
 '(ac-trigger-key "TAB")
 '(ac-use-menu-map t)
 '(bmkp-last-as-first-bookmark-file "/home/xdev/sync/INF/bookmarks")
 '(bookmark-default-file "~/sync/INF/bookmarks")
 '(coffee-tab-width 2)
 '(column-number-mode t)
 '(company-idle-delay 0.1)
 '(company-minimum-prefix-length 1)
 '(cua-rectangle-mark-key (kbd "<C-S-return>"))
 '(custom-safe-themes
   '("4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" default))
 '(ecb-options-version "2.40")
 '(ede-project-directories '("/home/xdev/.emacs.d"))
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(elfeed-feeds '("http://www.yaplakal.com/news.xml"))
 '(emms-mode-line-mode-line-function 'emms-mode-line-icon-function)
 '(face-font-family-alternatives
   '(("arial black" "arial" "DejaVu Sans")
     ("arial" "DejaVu Sans")
     ("verdana" "DejaVu Sans")))
 '(flycheck-clang-language-standard "c++20")
 '(flycheck-gcc-language-standard "c++20")
 '(flycheck-python-pycompile-executable "python3.6")
 '(inhibit-startup-screen t)
 '(ispell-dictionary nil)
 '(markdown-indent-on-enter t)
 '(org-agenda-files nil)
 '(package-selected-packages
   '(lsp-pyright org-roam etags undo-tree which-key auto-dim-other-buffers eglot rustic exec-path-from-shell graphviz-dot-mode helm-ls-git solarized-theme helm-css-scss alert js-mode lsp-javascript-typescript flycheck-gometalinter go-mode po-mode el-get terraform-mode scss-mode rope sage auto-complete-config tex-mik tex-site tex-buf tex dired-details dired-x dired-extension dired helm-files helm vue-mode matlab-mode hcl-mode keychain-environment company-racer company ediprolog helm-flycheck indium tide cmake-mode csharp-mode cargo dockerfile-mode racer toml-mode flycheck-rust rust-mode ac-html ag auctex helm-dash god-mode restart-emacs ggtags helm-gtags neotree helm-bm xah-elisp-mode zencoding-mode zenburn-theme zeal-at-point yari yaml-mode wrap-region windsize web-mode w3m w3 volatile-highlights sr-speedbar sourcemap soundklaus sml-mode smex smeargle smart-tabs-mode smart-tab sass-mode sage-shell-mode pysmell pymacs puppet-mode pony-mode persp-projectile paxedit openwith notmuch nlinum nginx-mode mustache-mode multi-term markdown-mode+ magit lua-mode logstash-conf kite json-mode js3-mode js2-refactor jquery-doc jinja2-mode jedi-direx jabber ipython image-dired+ iedit idomenu ido-vertical-mode helm-swoop helm-projectile-all helm-projectile helm-package helm-git-grep helm-ag gitignore-mode github-browse-file geiser frame-cmds flycheck find-file-in-repository f expand-region evil esup ess ensime emmet-mode elpy elisp-slime-nav elfeed ecb dirtree dired-ranger dired-narrow dired-filter dired-details+ ctags crontab-mode comment-dwim-2 coffee-mode butler buffer-move bookmark+ bm bbdb bash-completion auto-compile apache-mode anaphora ack ace-window ace-jump-buffer ac-helm))
 '(preview-gs-options
   '("-q" "-dNOPAUSE" "-DNOPLATFONTS" "-dPrinted" "-dTextAlphaBits=4" "-dGraphicsAlphaBits=4"))
 '(rustic-lsp-setup-p nil)
 '(safe-local-variable-values
   '((python-sort-imports-on-save t)
     (c-noise-macro-with-parens-names "IF_LINT")
     (eval when
           (and
            (buffer-file-name)
            (file-regular-p
             (buffer-file-name))
            (string-match-p "^[^.]"
                            (buffer-file-name)))
           (emacs-lisp-mode)
           (unless
               (featurep 'package-build)
             (let
                 ((load-path
                   (cons ".." load-path)))
               (require 'package-build)))
           (package-build-minor-mode))))
 '(send-mail-function nil)
 '(term-default-bg-color "#000000")
 '(term-default-fg-color "#dddd00")
 '(tool-bar-mode nil)
 '(tramp-default-host "localhost")
 '(tramp-default-user "root")
 '(tramp-encoding-shell "/bin/bash")
 '(truncate-lines t)
 '(warning-suppress-types '((comp)))
 '(wg-mode-line-decor-left-brace "[")
 '(wg-mode-line-decor-right-brace "]")
 '(wg-restore-position t)
 '(wg-switch-to-first-workgroup-on-find-session-file t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ace-jump-face-foreground ((t (:background "dim gray" :foreground "white"))))
 '(bm-face ((t (:background "dark slate blue" :foreground "gray59"))))
 '(bold ((t (:weight bold))))
 '(dired-symlink ((t (:inherit font-lock-keyword-face :weight normal))))
 '(ediff-current-diff-C ((t (:background "#888833" :foreground "black"))))
 '(ediff-even-diff-C ((t (:background "gray20" :foreground "dark gray"))))
 '(ediff-fine-diff-B ((t (:background "#22aa22" :foreground "black"))))
 '(ediff-odd-diff-C ((t (:background "midnight blue" :foreground "White"))))
 '(font-lock-keyword-face ((t (:foreground "#859900" :weight bold))))
 '(helm-candidate-number ((t (:background "deep sky blue" :foreground "black"))))
 '(helm-selection ((t (:foreground "lime green" :weight bold))))
 '(helm-source-header ((t (:foreground "gray" :weight normal :height 1.3 :family "Sans Serif"))))
 '(helm-swoop-target-word-face ((t (:background "dark slate blue" :foreground "#ffffff"))))
 '(helm-visible-mark ((t (:background "#005500" :foreground "black"))))
 '(magit-diff-none ((t (:inherit diff-context :foreground "dim gray"))))
 '(match ((t (:background "dark slate gray"))))
 '(mode-line ((t (:background "#403048" :foreground "#C4C9C8" :weight normal))))
 '(mode-line-buffer-id ((t (:foreground "coral" :weight bold))))
 '(mode-line-highlight ((t (:foreground "navajo white" :box nil :weight bold))))
 '(mode-line-inactive ((t (:inherit mode-line :background "#073642" :foreground "#586e75" :weight normal))))
 '(org-agenda-date-today ((t (:inherit org-agenda-date :foreground "lawn green" :slant normal :weight bold))))
 '(region ((t (foreground: "black" :background "dim gray" :inverse-video t))))
 '(vertical-border ((((type tty)) (:inherit mode-line-inactive))))
 '(web-mode-html-attr-name-face ((t (:foreground "medium sea green"))))
 '(web-mode-html-attr-value-face ((t (:inherit font-lock-string-face :foreground "gold3"))))
 '(web-mode-html-tag-face ((t (:foreground "chartreuse4"))))
 '(wg-brace-face ((t (:foreground "deep sky blue" :weight normal))))
 '(wg-command-face ((t (:inherit font-lock-function-name-face :weight normal))))
 '(wg-divider-face ((t (:weight normal))))
 '(wg-filename-face ((t (:inherit font-lock-keyword-face :weight normal))))
 '(wg-mode-line-face ((t (:inherit font-lock-doc-face :foreground "deep sky blue" :weight normal))))
 '(wg-other-workgroup-face ((t (:inherit font-lock-string-face :weight normal)))))


(setq load-path (cons "~/.emacs.d/elisp" load-path))
(add-to-list 'load-path "~/.emacs.d/elisp/")
(add-to-list 'load-path "~/.emacs.d/elisp/extensions/")
(add-to-list 'load-path "~/.emacs.d/elisp/menu/")


;; (add-hook 'after-init-hook (lambda ()
;;                              (when (fboundp 'auto-dim-other-buffers-mode)
;;                                (auto-dim-other-buffers-mode t))))

;; use-package
;; (eval-when-compile
;;   (require 'use-package))
;; (require 'bind-key)
;; end of use-package config


;;(setq bbdb-file "~/bbdb")    ; "~/.bbdb"

;; (require 'init-variables)
(require 'init-packages)

(require 'init-common)
(require 'init-colorscheme)
(require 'init-windows-buffers)
(require 'init-org)
(require 'init-dired)
(require 'init-filemodes)

(require 'init-gpg)
;; (when (and (require 'init-gpg nil 'noerror))
;; ;;  (require 'init-irc))
;;   ;;  (require 'init-mail-gnus)
;;   ;;  ;;(require 'init-mail-wl)
;;   ;;  ;; Please set `wl-message-id-domain' to get valid Message-ID string
;;   )

;; LSP-server takes a lot of RAM and I usually open a lot of projects.
;; (require 'init-eglot)

(require 'init-lisp)
(require 'init-git)
(require 'init-css-sass-scss)
(require 'init-js)
(require 'init-web-mode)
(require 'init-cpp)
(require 'init-navigation)
(require 'init-flycheck)
(require 'init-tex)  ;; loading tex.el before can reset TeX-PDF-mode setting
(require 'init-f5)
(require 'init-bookmarks)
(require 'init-markdown)
(require 'init-python)
(require 'init-gettext)
;; (require 'init-go)
(require 'init-navigation)        ; move to variables, functions, classes
(require 'init-search)            ; search text: in buffers, files, dirs
(require 'init-autocomplete)

(require 'init-tab)               ; TAB key, yasnippet
(require 'init-auto-insert)       ; needs yasnippet

(require 'init-yaml)
(require 'init-pascal)
(require 'init-lua)
(require 'init-typescript)

(require 'init-rust)

(require 'init-misc)

;; (require 'menu)

(provide 'init)
;;; init.el ends here
