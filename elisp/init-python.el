;;; init-python --- Python config  -*- lexical-binding: t; -*-
;;; Commentary:
;; sudo -H pip install pyright  # language server
;;; Code:

(require 'python)
(require 'flycheck)

;; Ipython integration with fgallina/python.el
(defun my-setup-ipython ()
  "Setup ipython integration with python-mode"
  (interactive)
  (setq
   python-shell-interpreter "ipython"
   python-shell-interpreter-args "--profile=dev"
   python-shell-prompt-regexp "In \[[0-9]+\]: "
   python-shell-prompt-output-regexp "Out\[[0-9]+\]: "
   python-shell-completion-setup-code ""
   python-shell-completion-string-code "';'.join(get_ipython().complete('''%s''')[1])\n"))

(define-key python-mode-map (kbd "C-<up>")    'python-nav-backward-block)
(define-key python-mode-map (kbd "C-<down>")  'python-nav-forward-block)


;; yasnippets make indent wrong on tab
;; (yas--fallback) makes a problem
;; (yas--keybinding-beyond-yasnippet)
(defun my-python-hook ()
  "Function to run on python-mode-hook."
  (interactive)
  ;; (smart-tabs-mode-enable)
  ;;(setq indent-tabs-mode nil) ; nil - use spaces, t - tabs
  ;;(setq tab-width 4)
  ;;(set-variable 'py-indent-offset 4)
  ;;(setq python-indent 4)
  ;;(setq python-indent-offset 4)
  ;;(setq tab-width (default-value 'tab-width))
  (define-key python-mode-map "\C-m" 'newline-and-indent)
  ;;(define-key python-mode-map "\C-m" 'python-indent-line)
  ;;(define-key python-mode-map (kbd "TAB") 'python-indent-line)
  ;;(make-variable-buffer-local 'yas-fallback-behavior)
  ;;(setq 'yas-fallback-behavior '(python-indent-line . nil))
  ;;(set (make-local-variable 'yas-fallback-behavior) '(apply ,python-indent-line))
  ;;(set 'yas-fallback-behavior '(apply ,newline-and-indent))
  (setq python-indent-trigger-commands nil)
  ;;(setq python-indent-trigger-commands '(indent-for-tab-command yas-expand yas/expand))

  (flycheck-mode t)
  (setq flycheck-pylintrc "/usr/data/local2/src/pashinin.com/.pylintrc")
  (flycheck-select-checker 'python-pylint)
  )

(when (require 'init-smarttabs nil 'noerror)
  (add-hook 'python-mode-hook 'my-python-hook))


(defun my/python-mode-hook ()
  ;; (add-to-list 'company-backends 'company-jedi)
  ;; (remove 'company-jedi 'company-backends)
  (flycheck-mode t)
  (setq
   ;; flycheck-pylintrc "/usr/data/local2/src/pashinin.com/.pylintrc"
   ;; flycheck-pylintrc ".pylintrc"
   flycheck-python-pylint-executable "pylint3"
   flycheck-python-pycompile-executable "python3"
   )
  ;; (flycheck-select-checker 'python-pylint)
  )

(add-hook 'python-mode-hook 'my/python-mode-hook)


;; (use-package lsp-pyright
;;   :hook (python-mode . (lambda () (require 'lsp-pyright)))
;;   :init (when (executable-find "python3")
;;           (setq lsp-pyright-python-executable-cmd "python3")))


(defun my-magic-python (filename)
  "Run python shell with current FILENAME."
  (cond
   ((and (or (file-under-path "/var/www" filename)
             (file-under-path "/var/www_production" filename))
         (file-exists-p "/var/www/.../reload"))
    (shell-command "touch reload"))
   (t
    (python-shell-send-buffer t))
   ))

(provide 'init-python)
;;; init-python.el ends here
