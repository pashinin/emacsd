;;; init-typescript --- TypeScript configs
;;; Code:
;;; Commentary:

;; (require 'lsp-javascript-typescript)
;; (use-package lsp-javascript-typescript
;;   :ensure t
;;   :config
;;   (progn
;;     (setq
;;      ;; js2-highlight-level 3
;;      ;; js-indent-level 2
;;      ;; js2-basic-offset 2
;;      ;; js2-mode-show-parse-errors nil
;;      ;; js2-mode-show-strict-warnings nil
;;      typescript-indent-level 2
;;      )

;;     ;; (add-hook 'js-mode-hook #'lsp-javascript-typescript-enable)
;;     ;; (remove-hook 'js-mode-hook #'lsp-javascript-typescript-enable)

;;     ;; (add-hook 'js2-mode-hook #'lsp-javascript-typescript-enable)
;;     ;; (remove-hook 'js2-mode-hook #'lsp-javascript-typescript-enable)

;;     ;; (add-hook 'typescript-mode-hook #'lsp-javascript-typescript-enable) ;; for typescript support
;;     ;; (add-hook 'js3-mode-hook #'lsp-javascript-typescript-enable) ;; for js3-mode support
;;     ;; (remove-hook 'js3-mode-hook #'lsp-javascript-typescript-enable)
;;     ;; (add-hook 'rjsx-mode #'lsp-javascript-typescript-enable) ;; for rjsx-mode support
;;     ))


(defun setup-tide-mode ()
  (interactive)

  ;; Typescript Interactive Development Environment
  (tide-setup)

  (local-set-key (kbd "RET") 'newline-and-indent)

  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (setq typescript-indent-level 2)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1))

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

;; formats the buffer before saving
;; (add-hook 'before-save-hook 'tide-format-before-save)
(add-hook 'typescript-mode-hook #'setup-tide-mode)

(provide 'init-typescript)
;;; init-typescript ends here
