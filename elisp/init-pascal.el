;;; init-pascal --- pascal config
;;; Commentary:
;;; Code:

(require 'pascal)
(setq pascal-indent-level 2)


(provide 'init-pascal)
;;; init-pascal.el ends here
