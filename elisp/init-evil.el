;;; init-evil --- Summary
;;; Commentary:
;;
;; v - visual mode (selecting by char)
;; V - visual line mode (selecting by line)
;; C-v - visual block selection (`rectangle-mark-mode' is better)
;; 
;; Search:
;; /init - search for "init"
;; 
;; 
;;; Code:

(use-package evil :ensure t)
(evil-mode 0)

(provide 'init-evil)
;;; init-evil.el ends here
