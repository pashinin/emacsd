;;; init-bookmarks --- bookmarks to files and dirs
;;; Commentary:
;;; Code:

(require 'init-variables)

;; Bookmarks inside 1 file
(use-package bm
  :ensure t
  :config
  (global-set-key (kbd "<s-return>") 'bm-toggle)
  (global-set-key (kbd "<s-next>")   'bm-next)
  (global-set-key (kbd "<s-prior>")  'bm-previous))

;; bookmark+ is not available in MELPA anymore
(let ((bookmarkplus-dir "~/.emacs.d/custom/bookmark-plus/")
      (emacswiki-base "https://www.emacswiki.org/emacs/download/")
      (bookmark-files '("bookmark+.el" "bookmark+-mac.el" "bookmark+-bmu.el" "bookmark+-key.el" "bookmark+-lit.el" "bookmark+-1.el")))
  (require 'url)
  (add-to-list 'load-path bookmarkplus-dir)
  (make-directory bookmarkplus-dir t)
  (mapcar (lambda (arg)
            (let ((local-file (concat bookmarkplus-dir arg)))
              (unless (file-exists-p local-file)
                (url-copy-file (concat emacswiki-base arg) local-file t))))
          bookmark-files)
  (byte-recompile-directory bookmarkplus-dir 0)
  (require 'bookmark+)
  (setq
   bookmark-default-file "~/sync/INF/bookmarks"
   ;; bmkp-last-as-first-bookmark-file "~/sync/INF/bookmarks"
   )
  )


;; (setq w32-pass-lwindow-to-system nil)
;; (setq w32-lwindow-modifier 'super) ; Left Windows key

;; (setq w32-pass-rwindow-to-system nil)
;; (setq w32-rwindow-modifier 'super) ; Right Windows key

;; bookmarks
(defun my-bookmarks-list-same-buffer ()
  "Open *Bookmarks* in current buffer."
  (interactive)
  (bookmark-bmenu-list)
  (switch-to-buffer "*Bookmark List*"))
(global-set-key (kbd "s-b") 'my-bookmarks-list-same-buffer)

(provide 'init-bookmarks)
;;; init-bookmarks.el ends here
