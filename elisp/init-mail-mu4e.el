;;; init-mail-mu4e --- Read email using mu4e
;;; Commentary:
;;
;; https://www.overleaf.com/articles/mu4e-refcard/ndxzcszfmwhc.pdf
;;
;; Overall schema:
;;   view -> index -> fetch
;;
;; What to use to fetch mail?
;;   * ???
;;   * isync (C lang)
;;   * offlineimap (python, old shit)
;; 
;; isync (executable name: mbsync):
;;   sudo apt install isync
;;   configuration: ~/.mbsyncrc
;;
;;   mbsync -a
;; 
;; mu4e
;;
;; mu init --maildir=~/Maildir --my-address=sergey@pashinin.com
;; 
;;
;; sudo apt install mu4e isync
;; 
;;; Code:

(require 'smtpmail)



(provide 'init-mail-mu4e)
;;; init-mail-mu4e.el ends here
