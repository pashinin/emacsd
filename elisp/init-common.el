;;; init-common --- my functions
;;; Commentary:
;;; Code:

(require 'init-variables)

;; Save history of minibuffer commands
(require 'savehist)
(setq savehist-file (concat my-emacs-files-dir "savehist"))  ; before activating mode
(savehist-mode 1)


(require 'init-common-windows)     ; change some variables for fucking Windows


;;--------------------------------------------------------------------
;; Common
(menu-bar-mode 0)              ; Hide menu: File, Edit, Options...
(tool-bar-mode 0)              ; hide buttons
(scroll-bar-mode -1)           ; hide scrollbars
(setq-default fill-column 72)  ; 80-char margin

;; (use-package wrap-region
;;   :ensure t
;;   ;;(add-hook 'ruby-mode-hook 'wrap-region-mode)   ; for specific mode
;;   :config
;;   (wrap-region-global-mode t))                     ; for all buffers

;; Navigate between buffer splits: super + arrows
(require 'windmove)  ; part of Emacs
;; (windmove-default-keybindings 's)
(windmove-default-keybindings 'H)

;;; Scroll buffer by 1 line - M-f, M-b
(global-set-key "\M-b" (lambda () (interactive) (scroll-down 1)))
(global-set-key "\M-f" (lambda () (interactive) (scroll-up 1)))

(column-number-mode 1)   ; display not only current line but also a column

;; display line numbers (packages "linum", "nlinum" are too old)
(when (version<= "26.0.50" emacs-version)
  (global-display-line-numbers-mode))

;; some settings
(setq inhibit-startup-message t)        ; Prevent the startup message
(setq scroll-step 1)       ;; scroll by 1 line
(global-hl-line-mode 1)    ;; highlight current line

(fset 'yes-or-no-p 'y-or-n-p)   ;; "y", "n" instead of "yes", "no"

(use-package multiple-cursors
  :ensure t
  :commands set-rectangular-region-anchor mc/edit-lines
  :bind (("C-S-c C-S-c" . mc/edit-lines)
         ("C-<" . mc/mark-previous-like-this)
         ("C->" . mc/mark-next-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ("s-SPC" . set-rectangular-region-anchor)))

(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))
;;(global-set-key (kbd "C-=") 'er/expand-region))  ;; select next logical block

;;;; when splitting - focus on new
(global-set-key "\C-x2" (lambda () (interactive)(split-window-vertically) (other-window 1)))
(global-set-key "\C-x3" (lambda () (interactive)(split-window-horizontally) (other-window 1)))

;; Upper/lower case ( C-x C-u  |  C-x C-l)
;; don't warn these commands
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;;(if (require 'page-break-lines nil 'noerror)
;;    (global-page-break-lines-mode))

;; Whitespaces
;;(require 'whitespace)
;;(setq whitespace-space 'underline)
(global-set-key (kbd "C-_") 'whitespace-mode)   ; show spaces
(defun whack-whitespace (arg)
  "Delete all white space from point to the next word.  With prefix ARG
    delete across newlines as well.  The only danger in this is that you
    don't have to actually be at the end of a word to make it work.  It
    skips over to the next whitespace and then whacks it all to the next
    word."
  (interactive "P")
  (let ((regexp (if arg "[ \t\n]+" "[ \t]+")))
    (re-search-forward regexp nil t)
    (replace-match "" nil nil)))
(global-set-key (kbd "s-_") 'whack-whitespace)   ; kill spaces to next word

;; reload file contents
(global-set-key (kbd "M-<kp-1>")    'revert-buffer)

;; undo/redo windows manipulation with "C-c left", "C-c right"
(winner-mode 1)

(delete-selection-mode 1)   ; delete seleted text when typing
(show-paren-mode t)         ; highlight brackets



(if (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode 0))

;; Make buffer names unique
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward     ;; filename:dirname
      uniquify-separator ":")

;; http://www.gnu.org/software/emacs/manual/html_node/emacs/Backup-Copying.html
(setq make-backup-files nil   ;; nil - do not make backup garbage files (file.txt~)
      backup-directory-alist '(("." . (concat my-emacs-files-dir "file_backups")))
      backup-by-copying nil
      backup-by-copying-when-mismatch t)

;; Unicode
;;(setq default-buffer-file-coding-system 'utf-8-unix)
(setq buffer-file-coding-system 'utf-8-unix
      default-file-name-coding-system 'utf-8-unix
      default-keyboard-coding-system 'utf-8-unix
      default-process-coding-system '(utf-8-unix . utf-8-unix))

;;(setenv "LC_ALL"     "en_US.UTF-8")
(set-language-environment 'UTF-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq file-name-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8)
;;(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))
;; MS Windows clipboard is UTF-16LE
;;(set-clipboard-coding-system 'utf-16le-dos)

;; when opening a file - try these encodings (bottom-up):
(prefer-coding-system 'cp866)
(prefer-coding-system 'koi8-r-unix)
(prefer-coding-system 'windows-1251-dos)
(prefer-coding-system 'utf-8-unix)

(global-set-key "\C-l" 'goto-line)   ; Goto-line key

;; lines wrapping
(setq truncate-lines 1)  ;; disable line wrap
(setq truncate-partial-width-windows nil)
(global-set-key [f12] 'toggle-truncate-lines) ; F12 to toggle line wrap

;; disable backups for editing "sudo su.."
(setq backup-enable-predicate
	  (lambda (name)
		(and (normal-backup-enable-predicate name)
			 (not
			  (let ((method (file-remote-p name 'method)))
				(when (stringp method)
				  (member method '("su" "sudo"))))))))

;; keys for russian layout (C-s == C-ы)
;;(global-set-key (kbd "C-п") (lookup-key global-map (kbd "C-g")))
;;(global-set-key (kbd "C-ч C-ы") (lookup-key global-map (kbd "C-x C-s")))
;;(global-set-key (kbd "C-ч C-с") (lookup-key global-map (kbd "C-x C-c")))
;;(global-set-key (kbd "C-ч C-у") (lookup-key global-map (kbd "C-x C-e")))

(defun kill-current-buffer ()
  "Kill current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defun kill-buffer-close-window ()
  "Kill current buffer and delete window."
  (interactive)
  (kill-buffer (current-buffer))
  (delete-window))

(global-set-key (kbd "<s-backspace>") 'kill-current-buffer)
(global-set-key (kbd "<s-delete>")    'kill-buffer-close-window)

(defun my-run-ack ()
  "Run ack-grep in current buffer."
  (interactive)
  (save-window-excursion
    (call-interactively 'rgrep))
  ;;(compile-goto-error
  (switch-to-buffer "*grep*"))
(global-set-key [S-f3] 'my-run-ack)
(global-set-key [C-f3] 'find-name-dired)
;; (find-name-dired)


;;
;; What to do on save?
;;
(defvar my-flag-delete-trailing-spaces t
  "If t trailing spaces will be removed on saving a file.")
(defun toggle-delete-trailing-spaces ()
  ""
  (interactive)
  (setq my-flag-delete-trailing-spaces (not my-flag-delete-trailing-spaces))
  (if my-flag-delete-trailing-spaces
      (message "Remove fucking trailing spaces when saving a file")
    (message "Do not touch spaces on save")))
(global-set-key (kbd "C-s-s") 'toggle-delete-trailing-spaces)

(defun unix-newlines-no-spaces ()
  "Convert current text file to a better format.
1. make unix lines endings \\n
2. delete spaces you don't need"
  (save-window-excursion
    (set-buffer-file-coding-system 'utf-8-unix)
    (if my-flag-delete-trailing-spaces
        (delete-trailing-whitespace))))
(add-hook 'before-save-hook 'unix-newlines-no-spaces)


(defun toggle-current-window-dedication ()
  (interactive)
  (let* ((window    (selected-window))
         (dedicated (window-dedicated-p window)))
    (set-window-dedicated-p window (not dedicated))
    (message "Window %sdedicated to %s"
             (if dedicated "no longer " "")
             (buffer-name))))

(global-set-key (kbd "<s-kp-enter>") 'toggle-current-window-dedication)


;; https://github.com/nonsequitur/smex
;; IDO to your recently and most frequently used "M-x" commands
;; (use-package smex
;; :ensure t
;;   :commands smex
;;   :init
;;   (progn
;;     (smex-initialize)
;;     (global-set-key (kbd "M-x") 'smex)
;;     (global-set-key (kbd "<menu>") 'smex)
;;
;;     (global-set-key (kbd "<menu>") 'helm-M-x)
;;     ))

(electric-pair-mode 1)

;; (use-package undo-tree
;;   :ensure t
;;   :commands global-undo-tree-mode
;;   :config
;;   (global-undo-tree-mode 1))

(setq byte-compile-warnings '(not nresolved
                                  free-vars
                                  callargs
                                  redefine
                                  obsolete
                                  noruntime
                                  cl-functions
                                  interactive-only
                                  ))

(use-package init-os-misc
  ;; :commands my-restart-emacs
  ;; :bind ("<s-pause>" . my-restart-emacs)
  )

(require 'ert)


(use-package dash
  :ensure t
  :config
  (global-dash-fontify-mode))

;; (use-package evil
;; :ensure t
;;   :init
;;   (progn
;;     ;;(setq evil-)
;;     ))

;; https://github.com/remyferre/comment-dwim-2
(use-package comment-dwim-2
  :ensure t
  :config
  (global-set-key (kbd "M-;") 'comment-dwim-2))



(global-set-key (kbd "C-x C-f") 'ido-find-file)

(provide 'init-common)
;;; init-common.el ends here
