;;; init-mail --- Work with mail
;;; Commentary:
;;
;; gnus
;; mu4e  <-- (mu for Emacs, use it)
;; mutt
;; notmuch
;; wanderlust
;; 
;;; Code:

;; Tell Emacs to use GNUTLS instead of STARTTLS to authenticate when
;; sending mail.
;; (setq starttls-use-gnutls t)

;; Tell Emacs about your mail server and credentials
(require 'smtpmail)

(setq
 user-full-name "Sergey Pashinin"
 user-mail-address "sergey@pashinin.com"
 smtpmail-smtp-server "pashinin.com"
 send-mail-function 'smtpmail-send-it
 ;; smtpmail-smtp-service 587
 )



(provide 'init-mail)
;;; init-mail.el ends here
