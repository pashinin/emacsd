;;; init-math.el --- Math
;;; Commentary:
;;
;; Sage shell:
;; https://github.com/stakemori/sage-shell-mode
;;
;;; Code:

(use-package sage-shell-mode)
;;


(provide 'init-math)
;;; init-math.el ends here
