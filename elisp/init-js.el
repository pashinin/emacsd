;;; init-js-coffee.el --- JavaScript / CoffeeScript
;;; Commentary:
;;
;; https://github.com/mooz/js2-mode (can't use it because vue-mode uses
;; mmm-mode and js2-mode does not work with it:
;; https://github.com/mooz/js2-mode/issues/124)
;;
;;
;; https://github.com/thomblake/js3-mode (abandoned)
;;
;; sudo apt-get install nodejs npm
;; sudo npm install -g jslint
;;
;; https://github.com/marijnh/tern
;;
;;; Code:

(require 'init-variables)

(require 'js)
(add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
(setq
 js-indent-level 2
 )

;; js2
;; https://emacs.stackexchange.com/questions/26949/can-i-turn-off-or-switch-the-syntax-checker-for-js2-mode
;;
;; I disabled js2-mode since I use vue-mode (which uses mmm-mode) and
;; js2-mode does not work with it:
;;
;; https://github.com/mooz/js2-mode/issues/124
;;
;; (use-package js2-mode
;;   :ensure t
;;   :init
;;   (progn
;;     (add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
;;     (setq
;;      js2-highlight-level 3
;;      js-indent-level 2
;;      js2-basic-offset 2
;;      js2-mode-show-parse-errors nil
;;      js2-mode-show-strict-warnings nil
;;      )

;;     (add-hook 'js2-mode-hook
;;               (lambda ()
;;                 (flycheck-mode t)
;;                 (setq
;;                  indent-tabs-mode nil
;;                  js-indent-level 2
;;                  js2-basic-offset 2
;;                  js2-mode-show-parse-errors nil
;;                  js2-mode-show-strict-warnings nil)
;;                 ))
;;     ))

;; (use-package js2-refactor
;;   :ensure t)

;;(add-to-list 'load-path (concat my-emacs-ext-dir "jquery-doc"))
(add-to-list 'load-path "~/src/lintnode")

(use-package init-tab)

(provide 'init-js)
;;; init-js.el ends here
