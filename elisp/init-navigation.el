;;; init-navigation --- Tools for navigation
;;; Commentary:
;;
;; By navigation I mean language specific names like function and
;; variables. For searching text or file names - see `init-search.el'
;;
;;; Code:

;; For Elisp
;; https://github.com/purcell/elisp-slime-nav
(use-package elisp-slime-nav
:ensure t
  :config
  ;; elisp-slime-nav-mode-map
  (dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
    (add-hook hook 'turn-on-elisp-slime-nav-mode)))

;; (use-package ctags
;;   :ensure t
;;   :bind ("<f7>" . ctags-create-or-update-tags-table)
;;   :config
;;   (setq path-to-ctags my-emacs-files-dir  ;; <- your ctags path here
;;         tags-revert-without-query t))
;;(global-set-key (kbd "<f7>") 'ctags-create-or-update-tags-table)

;; (use-package etags
;;   :ensure t
;;   :bind (("C-." . find-tag)
;;          ("M-." . pop-tag-mark)))
;;(global-set-key (kbd "C-.") 'find-tag)
;;(global-set-key (kbd "M-.") 'pop-tag-mark)






(provide 'init-navigation)
;;; init-navigation.el ends here
