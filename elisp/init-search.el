;;; init-search.el --- Searching stuff in buffers, files, filesystem
;;; Commentary:
;;
;; Search
;; 1. Text in buffers
;; 2. Text in files contents
;; 3. Files by name
;;
;;; Code:

(require 'eieio)  ; fix https://github.com/emacs-helm/helm/issues/815
(defun class-slot-initarg (class-name slot)
  (eieio--class-slot-initarg (eieio--class-v class-name) slot))

;; Helm - search anything
;; To configure colors: M-x customize-group <RET> helm <RET>
(require 'helm)
;; (setq
;;  helm-idle-delay 0.1
;;  helm-input-idle-delay 0.1
;;  helm-quick-update t
;;  helm-M-x-requires-pattern nil
;;  helm-ff-transformer-show-only-basename nil     ;; show full path, need for helm-ls-git
;;  helm-ff-skip-boring-files t)
(global-set-key (kbd "C-x C-b") 'helm-mini)   ; usual Helm
(global-set-key (kbd "s-[") 'helm-mini)
(global-set-key (kbd "<S-s-insert>") 'helm-show-kill-ring)
;; (global-set-key [S-f3] 'helm-do-grep)
(global-set-key (kbd "M-x") 'helm-M-x)



;;
;;; Search text in a buffer
;;
;; C-s      `isearch-forward'
;; C-r      `isearch-backward'
;; C-S-s    `helm-swoop'           https://github.com/ShingoFukuyama/helm-swoop
;; (use-package helm-swoop
;;   :ensure t
;;   :commands helm-swoop-from-isearch helm-multi-swoop-all-from-helm-swoop
;;   helm-multi-swoop helm-multi-swoop-all helm-swoop-back-to-last-point
;;   :require helm
;;   :bind ("C-S-s" . helm-swoop)
;;   :init
;;   (progn
;;     ;;(define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
;;     (setq
;;      helm-swoop-split-direction 'split-window-horizontally
;;      helm-swoop-split-with-multiple-windows nil
;;      helm-swoop-speed-or-color nil
;;      helm-multi-swoop-edit-save t
;;      helm-swoop-split-with-multiple-windows nil)))
(use-package helm-swoop
  :ensure t
  :config
  (setq
   helm-swoop-split-direction 'split-window-horizontally
   helm-swoop-split-with-multiple-windows nil
   helm-swoop-speed-or-color nil
   helm-multi-swoop-edit-save t
   helm-swoop-split-with-multiple-windows nil)
  (global-set-key (kbd "C-S-s") 'helm-swoop))


;; Search files by file names
;;
;; C-S-f3     `helm-for-files'
;;
;;(require 'helm-files)
;; (use-package helm-files
;;   :ensure helm
;;   :require helm
;;   :commands helm-do-grep-1 my-helm-do-grep helm-for-files
;;   :bind (;;("<s-f3>" . my-helm-do-grep)
;;          ("<C-S-f3>" . helm-for-files))
;;   :config
;;   (progn
;;     ;; do not list:
;;     (if (boundp 'helm-boring-file-regexp-list)
;;         (loop for ext in '("\\.swf$" "\\.elc$" "\\.pyc$")
;;               do (add-to-list 'helm-boring-file-regexp-list ext)))
;;     ;; helm-boring-file-regexp-list
;;     (setq helm-for-files-preferred-list
;;           '(
;;             ;;helm-source-buffers-list
;;             ;;helm-source-recentf
;;             ;;helm-source-bookmarks
;;             ;;helm-source-file-cache
;;             helm-source-files-in-current-dir
;;             helm-source-locate
;;             ;;(eieio--class-p helm-locate-source)
;;             )
;;           helm-locate-command "locate %s -e -A -i %s")
;;     ))


;; SilverSearch - https://github.com/ggreer/the_silver_searcher
;; Install:
;;   sudo apt-get install silversearcher-ag     (Ubuntu)
;;   yay -S the_silver_searcher  (Arch Linux)
;;   yay -S ripgrep  (Arch Linux) - even faster than ag
;;
;; Use:
;; s-f3       `helm-do-ag' - search text in all files' (under current dir) content
(use-package helm-ag
  :ensure t
  :bind ("<s-f3>" . helm-do-ag)
  :bind ("C-c g" . grep-whole-git)
  :bind ("C-c C-g" . helm-do-grep-ag)

  ;; :bind ("C-s" . helm-do-ag-this-file)
  :config
  (defun grep-whole-git ()
    (interactive)
    (helm-grep-do-git-grep t))
  (setq
   ;; helm-ag-base-command "ag --nocolor --nogroup --ignore-case"
   helm-ag-base-command "rg --no-heading --color=never --colors 'match:fg:white' --ignore-case"
   ;; helm-ag-base-command

   ;; helm-grep-ag-command "rg --color=always --colors 'match:fg:black' --colors 'match:bg:white' --smart-case --no-heading --line-number %s %s %s"
   helm-grep-ag-command "rg --color=always --colors 'match:fg:white' --no-heading --ignore-case --line-number %s %s %s"
   ;; --smart-case


   helm-grep-ag-pipe-cmd-switches '("--colors 'match:fg:black'" "--colors 'match:bg:yellow'")

   ;; helm-ag-use-grep-ignore-list
   ;; helm-ag-command-option "--all-text"
   helm-ag-thing-at-point 'symbol
   helm-ag-source-type 'file-line))


(provide 'init-search)
;;; init-search.el ends here
