;;; init-dired --- Dired config
;;; Commentary:
;; install "dired-details+" using MELPA (hide file info browsing dir using "(")
;;; Code:

(require 'dired)

(setq dired-listing-switches "-alh")   ; human readable size (Kb, Mb, Gb)
;;(setq-default dired-omit-mode t)
;;(add-hook 'dired-mode-hook '(lambda () (toggle-truncate-lines 1)))
(defun my-dired-options-enable ()
  "My personal hook-function for `dired-mode'."
  (interactive)
  (toggle-truncate-lines 1)    ; do not wrap lines

  ;; marks
  (local-set-key (kbd "<kp-7>") 'dired-mark)
  (local-set-key (kbd "<kp-9>") 'dired-unmark)
  (local-set-key (kbd "* +")    'my-dired-mark-all-current-ext)
  (local-set-key (kbd "* -")    'my-dired-unmark-all-current-ext)
  (local-set-key (kbd "* 0")    'dired-unmark-all-marks)
  (local-set-key (kbd "* RET")  'my-dired-mark-all)
  ;;(local-set-key (kbd "M-h")    'dired-omit-mode)

  ;; (local-set-key (kbd "q")      'dired-jump)
  (local-set-key (kbd "q")      'dired-up-directory)

  ;;(if (eq system-type 'windows-nt)
  ;;    (local-set-key (kbd "s-d")    'dired-explorer)
  ;;  (local-set-key (kbd "s-d")    'dired-nautilus))
  (local-set-key (kbd "<C-kp-divide>") 'my-toggle-dired-dwim)
  (local-set-key (kbd "<kp-8>") 'dired-previous-line)
  (local-set-key (kbd "<kp-5>") 'dired-next-line)
  (local-set-key (kbd "<kp-4>") 'left-char)
  (local-set-key (kbd "<kp-6>") 'right-char)

  (local-set-key (kbd "Z") 'my-dired-compress)

  ;; (define-key dired-mode-map (kbd "M-<up>") nil)

  ;; move buffer up/down
  ;; (local-set-key (kbd "<M-kp-8>") "\C-u1\M-v")
  ;; (local-set-key (kbd "<M-kp-5>") "\C-u1\C-v")
  ;;(local-set-key (kbd "C-s-c") 'my-compress-pictures)

  (setq dired-omit-files "^#\\|~$")
  (setq dired-omit-files
        (concat dired-omit-files "\\|^\\.\\([^.].+\\)?$"))
  ;; (setq dired-omit-files
  ;;       (concat dired-omit-files "\\|^session\.\\([^.]\\{9,10\\}[^.].+\\)?$"))
  (setq dired-omit-files
        (concat dired-omit-files "\\|^__pycache__$"))
  (setq dired-omit-files
        (concat dired-omit-files "\\|,cover$"))
  (setq dired-omit-files
        (concat dired-omit-files "\\|\.retry$"))

  ;;(setq dired-omit-files
  ;;      (concat dired-omit-files "\\|^.+.js$"))
  (setq dired-omit-files
        (concat dired-omit-files "\\|^.+.map$"))
  ;;(dired-omit-mode 1)
  ;; (if (not (eq system-type 'windows-nt))
  ;;     (if (fboundp 'dired-hide-details-mode)
  ;;         (dired-hide-details-mode)))  ; hide all info, only filenames
  )

(add-hook 'dired-mode-hook 'my-dired-options-enable)
(define-key dired-mode-map (kbd "<backspace>") 'dired-up-directory)


(use-package dired-extension)
(use-package dired-x          ; omitting files possibility
  :commands dired-jump
  ;;(global-set-key (kbd "s-\\") 'dired-jump) ;; Jump to parent dir or dir from file
  ;;:bind ("s-\\" . dired-jump)
  :config
  (progn
    (global-set-key (kbd "s-\\") 'dired-jump)
    (global-set-key (kbd "s-/") 'dired-jump)
    ;; move to other opened dired window on renaming?
    (setq dired-dwim-target t)
    (defun my-toggle-dired-dwim ()
      "Switch 'rename-move' mode in dired.
Rename into the same dir or to the dir of other dired-window."
      (interactive)
      (setq dired-dwim-target (not dired-dwim-target)))
    ))


(use-package dired-details
;:ensure t
  :commands dired-details-hide)
(setq-default dired-details-hidden-string "---")
(if (eq system-type 'windows-nt)
    (use-package dired-details+))         ; for Windows
;;(use-package dired+)

;; (use-package init-openwith)
(require 'init-openwith)
(use-package init-dired-marks)
;; (use-package init-image-dired)

;(use-package init-dired-z)


;; Reload dired after making changes
(--each '(dired-do-rename
          dired-do-copy
          dired-create-directory
          wdired-abort-changes)
  (eval `(defadvice ,it (after revert-buffer activate)
           (revert-buffer))))


;;
;; KEYS and default settings
;;
(when (eq system-type 'windows-nt)
  (defun dired-explorer ()
    "Open Windows Explorer in current directory."
    (interactive)
    (if (eq major-mode 'dired-mode)
        (shell-command (concat "explorer \"" (replace-regexp-in-string "/" "\\\\" (dired-current-directory)) ""))
      (let ((path (file-name-directory (buffer-file-name))))
        (setq path (replace-regexp-in-string "/" "\\\\" path))
        (shell-command (concat "explorer \"" path "\""))))))

(if (eq system-type 'windows-nt)
    (global-set-key (kbd "s-d")    'dired-explorer)
  (global-set-key (kbd "s-d")    'dired-nautilus))

;; Solves UTF-8 problems that "find ... -ls" has
(setq find-ls-option '("-print0 | xargs -0 ls -alhd" . ""))


;; (require 'dired-async)
;(autoload 'dired-async-mode "dired-async.el" nil t)
;(dired-async-mode 1)

(provide 'init-dired)
;;; init-dired.el ends here
