;;; init-misc --- Random stuff
;;; Commentary:
;;; Code:

;; Shadow not active buffers.
;; Good for users that look at your screen.
(use-package auto-dim-other-buffers
  :ensure
  :init
  (auto-dim-other-buffers-mode t))


(provide 'init-misc)
;;; init-misc.el ends here
