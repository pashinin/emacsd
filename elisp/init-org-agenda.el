;;; init-org-agenda --- description
;;; commentary:
;;
;; C-c C-t      mark as TODO
;;
;;; Code:

(require 'org-agenda)

(setq org-agenda-span 'week)

(setq org-agenda-window-setup 'current-window)  ;; Type C-h v
;; org-agenda-window-setup
;; for other options.
;; do not display DONE tasks:
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-deadline-is-shown t)
(setq org-agenda-skip-scheduled-if-done t)


;; %c   the category of the item, "Diary" for entries from the diary,
;;      or as given by the CATEGORY keyword or derived from the file name
;; %e   the effort required by the item
;; %l   the level of the item (insert X space(s) if item is of level X)
;; %i   the icon category of the item, see ‘org-agenda-category-icon-alist’
;; %T   the last tag of the item (ignore inherited tags, which come first)
;; %t   the HH:MM time-of-day specification if one applies to the entry
;; %s   Scheduling/Deadline information, a short string
;; %b   show breadcrumbs, i.e., the names of the higher levels
;; %(expression) Eval EXPRESSION and replace the control string
;;      by the result
;;
;; " %i %-12:c%?-12t% s"
;;    %-12:c      %?-12t      % s
;; "  students:   15:30-17:30 TITLE"
(setq org-agenda-prefix-format '((agenda . "              %?-12t  %s")
                                 (timeline . " % s")   ; (timeline . "  % s")
                                 (todo . "")           ; (todo . " %i %-12:c")
                                 (tags . " %i %-12:c") ; (tags . " %i %-12:c")
                                 (search . " %i %-12:c")))
(setq
 org-agenda-use-time-grid nil
 org-agenda-time-grid '((daily today remove-match)
                        (1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2100)
                        "......" "----------------"))

;; show 7 days
(setq org-agenda-span 7)


;; ("Scheduled: " "Sched.%2dx: ")
(setq org-agenda-scheduled-leaders '("" "%2dx: "))



;; (org-agenda nil "n")  ; for running Agenda
;; See "org-agenda-custom-commands" variable for help

;; Update agenda buffer after saving "org-mode" file
;; http://stackoverflow.com/questions/3313210/converting-this-untabify-on-save-hook-for-emacs-to-work-with-espresso-mode-or-a
(defcustom my-update-agenda-on-save-modes '(org-mode)
  "When saving these modes - update Agenda buffer."
  :group 'my-vars)

(setq my-update-agenda-on-save-modes '(org-mode))  ;; on what modes saving?
(defun my-update-agenda-hook ()
  "When saving `org-mode' file - update Agenda buffer."
  (interactive)
  (when (member major-mode my-update-agenda-on-save-modes)
    (if (get-buffer org-agenda-buffer-name)
        (save-window-excursion
          (save-excursion  ; save cursor position
            (with-current-buffer org-agenda-buffer-name
              (let ((line (org-current-line)))
                (org-agenda-redo)
                ;;(if line (org-goto-line line))
                )))))))
(add-hook 'after-save-hook 'my-update-agenda-hook)



(provide 'init-org-agenda)
;;; init-org-agenda.el ends here
