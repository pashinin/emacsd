;;; init-go --- Go config
;;; Commentary:
;;; Code:

(use-package go-mode
  :ensure t)

(setq magit-auto-revert-mode nil)
(use-package flycheck-gometalinter
  ;; :bind ("C-s-g" . magit-status)
  :config
  (progn
    ;; skips 'vendor' directories and sets GO15VENDOREXPERIMENT=1
    (setq flycheck-gometalinter-vendor t)
    ;; only show errors
    (setq flycheck-gometalinter-errors-only t)
    ;; only run fast linters
    (setq flycheck-gometalinter-fast t)
    ;; use in tests files
    (setq flycheck-gometalinter-test t)
    ;; disable linters
    (setq flycheck-gometalinter-disable-linters '("gotype" "gocyclo"))
    ;; Only enable selected linters
    (setq flycheck-gometalinter-disable-all t)
    (setq flycheck-gometalinter-enable-linters '("~/go/bin/golint"))
    ;; Set different deadline (default: 5s)
    ;; (setq flycheck-gometalinter-deadline "10s")
    ;; Use a gometalinter configuration file (default: nil)
    ;; (setq flycheck-gometalinter-config "/path/to/gometalinter-config.json")
    (add-hook 'before-save-hook 'gofmt-before-save)

    (flycheck-gometalinter-setup)
    ))


(provide 'init-go)
;;; init-go.el ends here
