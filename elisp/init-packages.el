;;; init-packages --- Packages system for Emacs 24
;;; Commentary:
;;; Code:

(require 'init-variables)

(require 'package)
(setq package-archives '(
                         ;;("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/")
                         ("gnu"       . "http://elpa.gnu.org/packages/")))

;; (setq package-archives
;;       '(("melpa" . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/melpa/")
;;         ("org"   . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/org/")
;;         ("gnu"   . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/gnu/")
;;         ))

;;(setq package-load-list '((org nil) all))  ; do not load ORG-mode
(when (< emacs-major-version 27)
  (package-initialize))

;; auto install Melpa packages
(defvar my-packages)
(setq my-packages '(use-package))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

(provide 'init-packages)
;;; init-packages.el ends here
