;;; init-git --- Git config
;;; Commentary:
;;
;; C-s-g      `magit-status'
;; l l        short log
;; E          Rebase
;; M-n, M-p   reorder commits
;;
;; Windows (cygwin) has a bug (when using Magit "stash apply stash@{0}"):
;; it destroys brackets "{", "}" - so need to escape them
;; https://github.com/magit/magit/issues/522
;;
;;; Code:

(use-package magit
  :ensure t
  :bind ("C-s-g" . magit-status)
  :config
  (progn
    ;; To show "-S" (sign flag) when preparing a commit
    ;; It is hidden on level 5
    (setq transient-default-level 5)

    (setq
     magit-auto-revert-mode      nil
     magit-auto-revert-mode      nil
     ;; magit-set-upstream-on-push  t
     magit-commit-show-diff      nil
     ;; magit-display-buffer-function 'magit-display-buffer-traditional
     magit-display-buffer-function 'magit-display-buffer-same-window-except-diff-v1
     )
    )
  )


;; Emacs restart needed to take effect
(setq smerge-command-prefix (kbd "s-m"))


;; `helm-ls-git' help
;; ==================
;; C-]  -  Switch full path / basename
(use-package helm-ls-git :ensure t)
(global-set-key (kbd "s-]") 'helm-ls-git)


(provide 'init-git)
;;; init-git.el ends here
