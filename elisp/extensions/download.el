;;; download --- Download functions
;;; Commentary:
;;
;;; Code:


(require 'notify)


(defvar download-default-directory "~/Downloads")

(defun download-url (url)
  "Downloads URL into `default-directory'."
  (interactive)
  (let* ((name (file-name-nondirectory (directory-file-name url))))
    (notify "New download" name))
  ;; (shell-command (concat "curl -o 94aefd8e.svg " url " &"))
  )


(defun download-torrent (torrent)
  "Downloads TORRENT file into `download-default-directory'."
  (interactive)
  (let* ((name (file-name-nondirectory (directory-file-name torrent))))
    (notify "New torrent" name))
  ;; (shell-command (concat "curl -o 94aefd8e.svg " url " &"))
  )


(provide 'download)
;;; download.el ends here
