;;; init-cpp.el --- C/C++
;;; Commentary:
;; Copyright (C) Sergey Pashinin
;; Author: Sergey Pashinin <sergey@pashinin.com>
;;
;; apt-get install global
;;
;; Linux docs about C code style:
;; https://www.kernel.org/doc/html/latest/process/coding-style.html#you-ve-made-a-mess-of-it
;;
;; As a language server: ccls
;; sudo apt install ccls
;;
;;; Code:

;; (require 'init-smarttabs)
;;-----------------------------------------------------
;; C++
(require 'cc-vars)
(require 'cc-mode)
(require 'my-cpp)

(setq-default
 ;; c-indent-tabs-mode t     ; Pressing TAB should cause indentation
 ;; c-indent-level 4         ; A TAB is equivilent to four spaces
 ;; c-argdecl-indent 0       ; Do not indent argument decl's extra
 ;; c-tab-always-indent t
 ;; backward-delete-function nil
 ;; c-default-style "cc-mode"
 c-default-style '((java-mode . "java")
                   (awk-mode . "awk")
                   (other . "linux"))
 c-basic-offset 4
 )




;; If a statement continues on the next line, indent the continuation by 4
;; (c-add-style "baumanka" '((c-continued-statement-offset 4)))

;; (defun my-c-mode-hook ()
;;   "Set some C style params."
;;   (interactive)
;;   (c-set-style "my-c-style")  ; cc-mode, BSD, Ellemtel, linux
;;   ;;(if (string-match path (buffer-file-name))
;;   ;;                                 (c-set-style "linux"))
;;   (c-set-offset 'substatement-open '0) ; brackets should be at same indentation level as the statements they open
;;   (c-set-offset 'case-label '+)        ; indent case labels by c-indent-level, too

;;   (c-set-offset 'inline-open '+)
;;   (c-set-offset 'block-open '+)
;;   (c-set-offset 'brace-list-open '+)   ; all "opens" should be indented by the c-indent-level
;;   (setq c-basic-offset 4)
;;   (define-key c-mode-base-map "/" 'self-insert-command)  ;; do not break tabs when comment
;;   (define-key c-mode-base-map "*" 'self-insert-command)
;;   (local-set-key (kbd "RET") 'newline-and-indent)

;;   (c-set-offset 'arglist-intro '+)
;;   (c-toggle-auto-newline 1)
;;   )


;; The Language Server Protocol (LSP)
;; (use-package lsp-mode
;;   :ensure t
;;   :hook ((c-mode . lsp)
;; 	     (c++-mode . lsp)
;;          (lsp-mode . lsp-enable-which-key-integration))
;;   :commands lsp
;;   :config
;;   (setq lsp-keymap-prefix "C-c l")
;;   (define-key lsp-mode-map (kbd "C-c l") lsp-command-map)
;;   (setq lsp-file-watch-threshold 15000))

;; (use-package ccls
;;   :ensure t
;;   :config
;;   :hook ((c-mode c++-mode objc-mode cuda-mode) .
;;          (lambda () (require 'ccls) (lsp)))
;;   (setq ccls-executable "/usr/local/bin/ccls")
;;   (setq ccls-initialization-options
;; 	    '(:index (:comments 2) :completion (:detailedLabel t)))
;;   )

(provide 'init-cpp)
;;; init-cpp.el ends here
