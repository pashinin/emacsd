;;; init-openwith  --- description  -*- lexical-binding: t; -*-
;;; Commentary:
;; Install "openwith" from MELPA
;;; Code:

(require 'dired)
(use-package openwith
  :ensure t
  :commands (openwith-make-extension-regexp))
(setq
 openwith-associations
 (list
  ;; "pages" is like docx for Apple
  (list (openwith-make-extension-regexp
         '("odt" "ods" "doc" "docx" "xls" "xlsx" "rtf" "pptx" "ppt" "odp" "csv" "pages"))
        "libreoffice" '(file))
  (list (openwith-make-extension-regexp
         '("pdf" "dvi" "djvu"))
        ;; "evince" '(file)
        ;; "atril" '(file)
        "okular" '(file)
        )
  (list (openwith-make-extension-regexp
         '("rar" "tar.gz" "gz" "7z" "zip"))
        "file-roller" '(file))
  (list (openwith-make-extension-regexp
         '("ggb"))
        "geogebra" '(file))
  (list (openwith-make-extension-regexp
         '("chm"))
        "xchm" '(file))
  (list (openwith-make-extension-regexp
         '("iso"))
        "gnome-disk-image-mounter" '(file))
  (list (openwith-make-extension-regexp
         '("mp3" "ogg" "wav" "m4a" "flac"))
        "haruna" '(file))
  (list (openwith-make-extension-regexp
         '("kra"))
        "krita" '(file))
  (list (openwith-make-extension-regexp
         '("mpeg" "avi" "wmv" "flv" "mkv" "mp4" "webm" "ogv" "m4v" "pls" "vob" "mov"))
        "haruna" '(file))
  (list (openwith-make-extension-regexp
         '("jpg" "jpeg" "png" "gif" "jpeg" "bmp"))
        ;; "shotwell"
        ;; "nomacs"
        "qimgv"
        '(file))))
;; (setq openwith-associations nil)
(openwith-mode t)

(defun open-in-external-app ()
  "Open a current file or marked files in an external app."
  (interactive)
  (let (doIt cmd
             (myFileList
              (cond
               ((eq major-mode 'dired-mode) (dired-get-marked-files))
               (t (list (buffer-file-name))))))

    (setq doIt (if (<= (length myFileList) 5) t (y-or-n-p "Open more than 5 files? ")))

    (when doIt
      (cond
       ((eq system-type 'windows-nt)
        (mapc (lambda (fPath)
                ;;(defvar w32-shell-execute)
                ;;(w32-shell-execute "open" (replace-regexp-in-string "/" "\\" fPath t t))
                ) myFileList)
        )
       ((eq system-type 'darwin)
        (mapc (lambda (fPath)
                (let ((process-connection-type nil))
                  (start-process "" nil "open" fPath)))  myFileList))
       ((eq system-type 'gnu/linux)
        (openwith-open-unix "mpv" myFileList)
        )))))

;; (defun my-dired-smart-open ()
;;   "Open a file/several files in external apps."
;;   (interactive)
;;   (let ((myFileList
;;          (cond
;;           ((eq major-mode 'dired-mode) (dired-get-marked-files))
;;           (t (list (buffer-file-name)))
;;           )))
;;     (cond
;;      ((> (length myFileList) 1)
;;           (open-in-external-app))
;;      (t
;;       ;;(if (s-ends-with? ".iso" (car myFileList))
;;       ;;    (shell-command "gnome-disk-image-mounter file:///usr/data/disk_3/OS/ubuntu-14.04-desktop-amd64.iso")
;;       ;;  (dired-find-file))
;;       (dired-find-file)
;;       )
;;      )))

(provide 'init-openwith)
;;; init-openwith.el ends here
