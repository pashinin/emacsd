;;; init-eglot --- LSP client  -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; # C++
;; sudo apt install ccls
;;
;; # Python
;; sudo pip install python-lsp-server
;;
;;; Code:

(use-package eglot
  :ensure t
  :config

  ;; Turn off flymake.
  (add-hook 'eglot-managed-mode-hook (lambda () (flymake-mode -1)))

  (add-hook 'c-mode-hook 'eglot-ensure)
  (add-hook 'c++-mode-hook 'eglot-ensure)
  (add-hook 'python-mode-hook 'eglot-ensure)
  ;; (add-hook 'emacs-lisp-mode-hook 'eglot-ensure)

  (add-hook 'rust-mode-hook 'eglot-ensure)

  )

(provide 'init-eglot)
;;; init-eglot.el ends here
