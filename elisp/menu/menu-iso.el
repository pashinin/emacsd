;;; menu-iso --- Menu for ISO files
;;; Commentary:
;;
;;; Code:


(require 'transient)
(require 'magit)
(require 'download)

(defun menu-iso-download-arch ()
  "Downloads Arch Linux ISO file into `default-directory'."
  (interactive)
  ;; (let (
  ;;       ;; (url "https://pashinin.com/img/exam2.94aefd8e.svg")
  ;;       (url )
  ;;       )
  ;;   (shell-command (concat "curl -o 94aefd8e.svg " url " &")))
  (download-url "https://mirror.yandex.ru/archlinux/iso/2019.07.01/archlinux-2019.07.01-x86_64.iso")
  )

(define-transient-command menu-iso-new ()
  "Download new ISO menu."
  [
   ["Download"
    ("a" "Arch Linux 2019.07.01"   menu-iso-download-arch)
    ("u" "Ubuntu 18.04 LTS"        menu-iso-download-arch)
    ]
   ]
  (interactive)

  ;; (interactive (list (dired-get-filename t t)))
  ;; (transient-setup 'dired-file nil nil :scope filename)
  ;; (transient-setup 'dired-file nil nil)

  ;; (transient-setup 'magit-branch nil nil :scope branch)
  (transient-setup 'menu-iso-new nil nil :scope "iso")
  )

(provide 'menu-iso)
;;; menu-iso.el ends here
