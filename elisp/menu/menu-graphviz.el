;;; menu-graphviz --- Menu for .gv files
;;; Commentary:
;;
;;; Code:


(require 'transient)
(require 'magit)
(require 'download)

(defun compile-graphviz (file)
  (interactive (list (or (buffer-file-name)
                         (dired-get-filename t t))))
  (byte-compile-file file))

(define-transient-command menu-graphviz (file)
  [
   ["Graphviz file"
    ("c" "compile to .png"   compile-graphviz)
    ;; ("d" "Compile to .elc"   (lambda (file) (interactive (list file)) (byte-compile-file file)))
    ;; ("l" "local branch"      magit-branch-checkout)
    ;; (6 "o" "new orphan"      magit-branch-orphan)
    ]
   ]
  ;; (interactive (list (buffer-file-name)))
  (interactive (list (dired-get-filename t t)))
  (message file)
  (transient-setup 'menu-graphviz nil nil :scope file)
  )


(provide 'menu-graphviz)
;;; menu-graphviz.el ends here
