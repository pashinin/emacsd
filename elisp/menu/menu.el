;;; init-menu --- Git config
;;; Commentary:
;;
;;; Code:

(require 'transient)
(require 'magit)
(require 'menu-dired)
(require 'menu-file-el)
(require 'menu-graphviz)

(define-transient-command my-test (branch)
  "Add, configure or remove a branch."
  :man-page "git-branch"
  ["Variables"
   :if (lambda ()
         (and magit-branch-direct-configure
              (oref transient--prefix scope)))
   ("d" magit-branch.<branch>.description)
   ("u" magit-branch.<branch>.merge/remote)
   ("r" magit-branch.<branch>.rebase)
   ("p" magit-branch.<branch>.pushRemote)]
  [["Checkout"
    ("b" "branch/revision"   magit-checkout)
    ("l" "local branch"      magit-branch-checkout)
    (6 "o" "new orphan"      magit-branch-orphan)]
   [""
    ("c" "new branch"        magit-branch-and-checkout)
    ("s" "new spin-off"      magit-branch-spinoff)
    (5 "w" "new worktree"    magit-worktree-checkout)]
   ["Create"
    ("n" "new branch"        magit-branch-create)
    ("S" "new spin-out"      magit-branch-spinout)
    (5 "W" "new worktree"    magit-worktree-branch)]
   ["Do"
    ("C" "configure..."      magit-branch-configure)
    ("m" "rename"            magit-branch-rename)
    ("x" "reset"             magit-branch-reset)
    ("k" "delete"            magit-branch-delete)]]
  (interactive (list (magit-get-current-branch)))
  (transient-setup 'magit-branch nil nil :scope branch))



(define-suffix-command menu-directory.<directory> (branch)
  "Edit the description of BRANCH."
  :class 'magit--git-variable
  :transient nil
  :variable "branch.%s.description"
  ;; :variable "%s"
  (interactive (list (oref current-transient-prefix scope)))
  ;; (magit-run-git-with-editor "branch" "--edit-description" branch)
  )

;;;###autoload (autoload 'menu-directory "init-menu" nil t)
(define-transient-command menu-directory (directory)
  ["Variables"
   :if (lambda ()
         (and magit-branch-direct-configure
              (oref transient--prefix scope)))
   ;; ("d" menu-directory.<directory>.description)
   ("d" menu-directory.<directory>)
   ;; ("u" magit-branch.<branch>.merge/remote)
   ;; ("r" magit-branch.<branch>.rebase)
   ;; ("p" magit-branch.<branch>.pushRemote)
   ]
  [["Directory"
    ("a" "Archive %s"   my-test)]
    ;; ("l" "local branch"      magit-branch-checkout)
    ;; (6 "o" "new orphan"      magit-branch-orphan)
   ]
  (interactive (list (expand-file-name default-directory)))
  (transient-setup 'menu-directory nil nil :scope directory)
  )

;; (define-transient-command menu-dired (branch)
;;   "Add, configure or remove a branch."
;;   :man-page "git-branch"
;;   ["Variables"
;;    :if (lambda ()
;;          (and magit-branch-direct-configure
;;               (oref transient--prefix scope)))
;;    ("d" magit-branch.<branch>.description)
;;    ;; ("u" magit-branch.<branch>.merge/remote)
;;    ;; ("r" magit-branch.<branch>.rebase)
;;    ;; ("p" magit-branch.<branch>.pushRemote)
;;    ]
;;   [
;;    ["Checkout"
;;     ("b" "branch/revision"   my-test)
;;     ("l" "local branch"      magit-branch-checkout)
;;     (6 "o" "new orphan"      magit-branch-orphan)
;;     ]
;;    ;; [""
;;    ;;  ("c" "new branch"        magit-branch-and-checkout)
;;    ;;  ("s" "new spin-off"      magit-branch-spinoff)
;;    ;;  (5 "w" "new worktree"    magit-worktree-checkout)]
;;    ;; ["Create"
;;    ;;  ("n" "new branch"        magit-branch-create)
;;    ;;  ("S" "new spin-out"      magit-branch-spinout)
;;    ;;  (5 "W" "new worktree"    magit-worktree-branch)]
;;    ;; ["Do"
;;    ;;  ("C" "configure..."      magit-branch-configure)
;;    ;;  ("m" "rename"            magit-branch-rename)
;;    ;;  ("x" "reset"             magit-branch-reset)
;;    ;;  ("k" "delete"            magit-branch-delete)]
;;    ]
;;   (interactive (list (magit-get-current-branch)))

;;   ;; (interactive (list (dired-get-filename t t)))
;;   ;; (transient-setup 'dired-file nil nil :scope filename)
;;   ;; (transient-setup 'dired-file nil nil)

;;   ;; (transient-setup 'magit-branch nil nil :scope branch)
;;   (transient-setup 'menu-dired nil nil :scope branch)
;;   )


(defun menu-main ()
  ;; (interactive (menu-dired))
  ;; (print major-mode)
  (cond

   ((eq major-mode 'fundamental-mode)
    ;;(do-magic-with-file (buffer-file-name)))
    (do-magic-with-file (file-truename (buffer-file-name))))

   ((eq major-mode 'emacs-lisp-mode)
    (call-interactively 'menu-file-el))

   ((eq major-mode 'dired-mode)
    (menu-mode-dired)
    ;; (call-interactively 'menu-mode-dired)
    )

   ((eq major-mode 'graphviz-dot-mode)
    (call-interactively 'menu-graphviz)))

  ;; (menu-dired "Asd")
  )

(global-set-key (kbd "<menu>")    (lambda () (interactive) (menu-main)))

(provide 'menu)
;;; menu.el ends here
