;;; menu-file-el --- Menu for .7z files
;;; Commentary:
;;
;;; Code:


(require 'transient)
(require 'magit)
(require 'download)

(defun compile-el-file (file)
  (interactive (list (or (buffer-file-name)
                         (dired-get-filename t t))))
  (byte-compile-file file))

(define-transient-command menu-file-el (file)
  [
   ["Emacs Lisp file"
    ("c" "compile to .elc"   compile-el-file)
    ;; ("d" "Compile to .elc"   (lambda (file) (interactive (list file)) (byte-compile-file file)))
    ;; ("l" "local branch"      magit-branch-checkout)
    ;; (6 "o" "new orphan"      magit-branch-orphan)
    ]
   ["Project"
    ("n" "new branch"        magit-branch-create)
    ("S" "new spin-out"      magit-branch-spinout)
    (5 "W" "new worktree"    magit-worktree-branch)]
   ]
  ;; (interactive (list (buffer-file-name)))
  (interactive (list (dired-get-filename t t)))
  (message file)
  (transient-setup 'menu-file-el nil nil :scope file)
  )


(provide 'menu-file-el)
;;; menu-file-el.el ends here
