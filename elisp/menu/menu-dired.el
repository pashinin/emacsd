;;; menu-dired --- Menu for dired mode
;;; Commentary:
;;
;;; Code:

(require 'dired)
(require 'menu-iso)

;; (defun files-stats (files)
;;   "Return a list of (extension . count).
;; FILES - a list of files to analyze.
;; Case of extension string is lowered."
;;   (interactive)
;;   (let ((res '()) ext count)
;;     (dolist (el files res)
;;       (if (file-directory-p el)
;;           (setq ext "DIR")
;;         (setq ext (downcase (or (file-name-extension el) ""))))
;;       ;;(assoc "ext" res)       ;    (ext . count)
;;       (setq count (or (cdr (assoc ext res))
;;                       0))
;;       (setq res (delq (assoc ext res) res))
;;       (add-to-list 'res `(,ext . ,(+ count 1)))
;;       )))
;; (length (files-stats (list "asd" "aasd.a" "/tmp")))

(defun menu-mode-dired ()
  "Call different menus in `dired-mode'."
  (let* ((f (dired-get-filename t t))
        (ext (file-name-extension (or f "")))
        (files (dired-get-marked-files)))
    (save-window-excursion
      ;; TODO: analyze - what is selected in dired-mode
      (cond
       ((and f (= (length files) 1))  ; only 1 file selected
        ;; (do-magic-with-file f)

        (cond
         ((string= ext "7z")
          ;; (message "asd")
          (call-interactively 'menu-file-7z)
          )
         ((string= ext "pdf")
          (call-interactively 'menu-file-pdf))
         (t
          ;; (call-interactively 'menu-file-el)
          (message (concat "Unknown " ext)))
         )

        ;; (menu-file-el f)
        )
       ((> (length files) 1)          ; more than 1 file selected
        ;; (let ((stats (files-stats files))
        ;;       img art)
        ;;   (cond
        ;;    ((files-many-plus-one stats "avi" "jpg")
        ;;     (message "cover"))
        ;;    ((files-many-plus-one stats "mkv" "jpg")
        ;;     (message "cover"))
        ;;    ((files-many-plus-one stats "ogg" "jpg")
        ;;     (when (yes-or-no-p "Make this an album art for all OGG files?")
        ;;       (setq img (get-first-image-from-files files))
        ;;       (setq art (make-art-image img))
        ;;       (dolist (el (get-files-by-extension files "ogg"))
        ;;         (message el)
        ;;         (ogg-add-cover art el))))
        ;;    (t
        ;;     (message "Do magic on each file...")
        ;;     ;; (mapc 'do-magic-with-file files)
        ;;     )
        ;;    ))
        )

       ;; if inside "ISO" dir
       ;; (equal (compare-strings "iSo" nil nil "iso" nil nil t) t)
       ((s-equals?
         (file-name-nondirectory (directory-file-name default-directory))
         "iso")
        (call-interactively 'menu-iso-new)
        ;; (call-interactively 'menu-directory)
        ;; (call-interactively 'menu-file-el)
        )
       (t
        (call-interactively 'menu-directory)

        ;; nothing selected!
        ;; (message (expand-file-name default-directory))
        )))))

(provide 'menu-dired)
;;; menu-dired.el ends here
