;;; menu-file-el --- Menu for .7z files
;;; Commentary:
;;
;;; Code:


(require 'transient)
(require 'magit)
(require 'download)


(defun pdf-extract-to-file(range newfile)
  (interactive
   (let ((range (read-string "Pages: " nil 'my-history))
         (file (read-string "Save as: " nil 'my-history)))
     (list range file)))
  ;; (read-string "Enter your name:")
  (message "pdftk full-pdf.pdf cat %s output %s.pdf" range newfile))

(define-transient-command menu-file-pdf (file)
  [
   ["PDF"
    ("e" "Extract pages to separate file"   pdf-extract-to-file)
    ]
   ;; ["Project"
   ;;  ("n" "new branch"        magit-branch-create)
   ;;  ("S" "new spin-out"      magit-branch-spinout)
   ;;  (5 "W" "new worktree"    magit-worktree-branch)]
   ]
  ;; (interactive (list (buffer-file-name)))
  (interactive (list (dired-get-filename t t)))
  (message file)
  (transient-setup 'menu-file-pdf nil nil :scope file)
  )


(provide 'menu-file-pdf)
;;; menu-file-pdf.el ends here
