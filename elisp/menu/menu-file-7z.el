;;; menu-file-el --- Menu for .7z files
;;; Commentary:
;;
;;; Code:


(require 'transient)
(require 'magit)
(require 'download)

(defun sudo-shell-command (command)
  (interactive "MShell command (root): ")
  (with-temp-buffer
    (cd "/sudo::/")
    (async-shell-command command)))

(defun install-7z ()
  (interactive)
  (sudo-shell-command "apt -y install p7zip-full"))

(define-transient-command menu-file-7z (file)
  [
   ["7z archive"
    ("e" "Extract here"   compile-el-file)
    ("i" "Install p7zip-full"   install-7z)
    ]
   ;; ["Project"
   ;;  ("n" "new branch"        magit-branch-create)
   ;;  ("S" "new spin-out"      magit-branch-spinout)
   ;;  (5 "W" "new worktree"    magit-worktree-branch)]
   ]
  ;; (interactive (list (buffer-file-name)))
  (interactive (list (dired-get-filename t t)))
  (transient-setup 'menu-file-7z nil nil :scope file)
  )


(provide 'menu-file-7z)
;;; menu-file-7z.el ends here
