;;; init-markdown.el --- Markdown
;;; Commentary:
;;; Code:

(use-package markdown-mode
  :ensure t
  :config
  (global-set-key (kbd "M-;") 'comment-dwim-2)
  (define-key markdown-mode-map (kbd "C-b") 'markdown-insert-bold)
  (define-key markdown-mode-map (kbd "RET") 'newline-and-indent))

(provide 'init-markdown)
;;; init-markdown.el ends here
