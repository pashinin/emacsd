;;; init-os-misc --- description  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'dired)
(use-package s
  :ensure t
  :commands (s-starts-with?))

(defun toggle-maximize ()
  "Maximize Emacs window."
  (interactive)
  (if (eq system-type 'windows-nt)
      (if (fboundp 'w32-send-sys-command) ;; if we really have this function
          (w32-send-sys-command 61488))
    (when (fboundp 'x-send-client-message)
      (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                             '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
      (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                             '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
      )))

(defun toggle-fullscreen (&optional f)
  (interactive)
  (let ((current-value (frame-parameter nil 'fullscreen)))
    (if (equal 'fullboth current-value)
        (set-frame-parameter nil 'fullscreen 'maximized)
      (set-frame-parameter nil 'fullscreen 'fullboth))))
(global-set-key [f11] 'toggle-fullscreen)

;; all created frames - fullscreen
;;(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; (defun emacs-process-p (pid)
;;   "If PID is the process ID of an Emacs process, return t, else nil.
;; Also returns nil if PID is nil."
;;   (when pid
;;     (let* ((cmdline-file (concat "/proc/" (int-to-string pid) "/cmdline")))
;;       (when (file-exists-p cmdline-file)
;;         (with-temp-buffer
;;           (insert-file-contents-literally cmdline-file)
;;           (goto-char (point-min))
;;           (search-forward "emacs" nil t)
;;           pid)))))

;; (defadvice desktop-owner (after pry-from-cold-dead-hands activate)
;;   "Don't allow dead emacsen to own the desktop file."
;;   (when (not (emacs-process-p ad-return-value))
;;     (setq ad-return-value nil)))


;; -----

;; This is quite hacky, and I'm not sure how reliable it is, but the
;; value gc-cons-threshold depends on the underlying word size. It's
;; 800000 on 64-bit systems, and 400000 on 32-bit systems.
(defconst 32-bit (= gc-cons-threshold 400000))
(defconst 64-bit (= gc-cons-threshold 800000))

;; (defun is-64bit-os ()
;;   "Return t if we are under 64 bit OS."
;;   (interactive)
;;   (if (eq system-type 'windows-nt)
;;       (file-directory-p "C:/Program files (x86)")
;;     nil))

;; C:\Program Files (x86)\WinCDEmu\vmnt64.exe D:\ubuntu-13.04-server-amd64.iso
(defun mount-iso (filename)
  "Mount ISO image FILENAME."
  (interactive)
  (let (p cmd)
    (cond
     ((eq system-type 'windows-nt)
      (setq p (if 64-bit "C:/Program Files (x86)/WinCDEmu/vmnt64.exe"
                "C:/Program Files (x86)/WinCDEmu/vmnt.exe"))
      (if (not (file-exists-p p)) (error "Install WinCDEmu"))
      (setq p (shell-quote-argument p))
      (shell-command (concat p " " (shell-quote-argument filename))))
     (t
      ;;(error "Don't know the system to use mounting")
      ;;fuseiso example.iso example
      (setq cmd (concat "fuseiso " (shell-quote-argument filename)))
      (shell-command cmd)
      ))))


(defcustom var-libreoffice-exe nil
  "Explicitly point to LibreOffice executable."
  :group 'my-vars)

(defun libreoffice-exec ()
  "Return a full path to LibreOffice executable."
  (interactive)
  (if var-libreoffice-exe
      var-libreoffice-exe
    (let (files res)
      (if (eq system-type 'windows-nt)
          (progn
            (setq files (list
                         "C:/Program Files (x86)/LibreOffice 4/program/soffice.exe"
                         "C:/Program Files (x86)/LibreOffice 4.0/program/soffice.exe"))
            (dolist (element files res)
              (if (file-exists-p element)
                  (setq res element))))
        "libreoffice"))))

;; doesn't work on windows
(defun libreoffice-convert (filename format)
  "Convert fucking doc FILENAME to FORMAT."
  (interactive)
  (message "converting")
  (let (office cmd)
    (setq office (libreoffice-exec))

    (setq cmd (concat (shell-quote-argument office)
                      " --headless"
                      " --invisible"
                      " --convert-to " format
                      " "
                      ;;" --outdir D:/"
                      (shell-quote-argument filename)
                      ;;filename
                      ))
    (message (shell-command-to-string cmd))
    ;;(message cmd)
    ;;(kill-ring-save cmd)
    ))

(defun files-stats (files)
  "Return a list of (extension . count).
FILES - a list of files to analyze.
Case of extension string is lowered."
  (interactive)
  (let ((res '()) ext count)
    (dolist (el files res)
      (if (file-directory-p el)
          (setq ext "DIR")
        (setq ext (downcase (or (file-name-extension el) ""))))
      ;;(assoc "ext" res)       ;    (ext . count)
      (setq count (or (cdr (assoc ext res))
                      0))
      (setq res (delq (assoc ext res) res))
      (add-to-list 'res `(,ext . ,(+ count 1)))
      )))
;; (length (files-stats (list "asd" "aasd.a" "/tmp")))

(defun files-many-plus-one (stats many one)
  "Return t if all files in STATS have ext MANY + 1 ONE.
STATS is returned by `files-stats'."
  (interactive)
  (when (= (length stats) 2)
    (when (and (assoc many stats)
               (assoc one stats))
      (= (cdr (assoc one stats)) 1))))

(defun get-first-image-from-files (files)
  "Return the first filename from FILES that is an image.
That ends with .jpg or .png."
  (interactive)
  (catch 'loop
    (let (res ext)
      (dolist (el files res)
        ;;(info "(elisp) Examples of Catch")
        (setq ext (downcase (or (file-name-extension el) "")))
        (when (or (string= ext "jpg")
                  (string= ext "jpeg"))
          (setq res el)
          (throw 'loop el))))))
;; (get-first-image-from-files (list "asd" "asd.jpg" "aasd.ogG"))

(defun get-files-by-extension (files ext)
  "Return a sublist from FILES where extension is EXT."
  (interactive)
  (let (res e)
    (dolist (el files res)
      (setq e (downcase (or (file-name-extension el) "")))
      (when (string= e ext)
        (if (not res)
            (setq res (list el))
          (push el res))))))
;; (get-files-by-extension (list "asd" "asd.jpg" "aasd.ogG" "aa.ogg") "ogg")


(defun file-under-path (path &optional filename)
  "Return t if PATH has a FILENAME in any folder under it."
  (interactive)
  (let* ((p (expand-file-name path)))
    (if filename
        (s-starts-with? p filename)
      (let ((file (expand-file-name (or buffer-file-name default-directory))))
        (s-starts-with? p file)
        ))))
;; (file-under-path "/d")
;; (file-under-path "/d" "/d/a.txt")

(provide 'init-os-misc)
;;; init-os-misc.el ends here
