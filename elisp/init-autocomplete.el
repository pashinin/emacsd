;; init-autocomplete --- Auto Completion
;;; Commentary:
;; Extensions: "auto-complete", "company".  Use 2nd (easier).
;; http://company-mode.github.io/
;;
;; Company doesn't have a way to show documentation in a popup.  Instead
;; you see the first line of the docstring in the echo area, and you can
;; press <f1> to pop up a window with documentation.
;;
;;; Code:

(require 'init-common)

(use-package company
  :ensure t
  :config
  (setq company-global-modes '(not pascal-mode))
  (global-company-mode)
  )

(provide 'init-autocomplete)
;;; init-autocomplete.el ends here
