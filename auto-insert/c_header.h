#ifndef ${1:`(upcase (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))`}_H
#define $1_H

$0

#endif /* $1_H */
