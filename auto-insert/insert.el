;;; ${1:`(file-name-nondirectory (file-name-sans-extension (buffer-file-name)))`} --- Summary
;;; Commentary:
;;; Code:

$0

(provide '$1)
;;; $1.el ends here
